import { ref } from 'vue'
import { defineStore } from 'pinia'

interface AddressBook {
  id: number
  name: string
  tel: string
  gender: string
}
export const useAddressBookStore = defineStore('counter', () => {

const address = ref<AddressBook>({
    id: 0,
    name: '',
    tel: '',
    gender: 'Male'
})
let lastId = 1
const addressList = ref<AddressBook[]>([])
const isAddNew = ref(false)
function save() {
    if (address.value.id > 0) {
        const editedIndex = addressList.value.findIndex((item) => item.id === address.value.id)
        addressList.value[editedIndex] = address.value
    } else {
        addressList.value.push({ ...address.value, id: lastId++ })
    }
    address.value = {
        id: 0,
        name: '',
        tel: '',
        gender: 'Male'
    }
    
}

function edit(id: number) {
    isAddNew.value = true
    const editedIndex = addressList.value.findIndex((item) => item.id === id)
    address.value = JSON.parse(JSON.stringify(addressList.value[editedIndex]))
}

function remove(id: number) {
    const reomvedIndex = addressList.value.findIndex((item) => item.id === id)
    addressList.value.splice(reomvedIndex, 1)
}

function cancel() {
    isAddNew.value = false
    address.value = {
        id: 0,
        name: '',
        tel: '',
        gender: 'Male'
    }
}
  return { address, addressList, save, isAddNew, edit, remove, cancel }
})
